﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Hosting;
using System.Web.Mvc;

namespace OnlineTraining.Common
{
    public class VideoDataResult: ActionResult
    {
        public string filename = null;

        public VideoDataResult(string param)
        {
            filename = param;

        }


        /// <summary>
        /// The below method will respond with the Video file
        /// </summary>
        /// <param name="context"></param>
        public override void ExecuteResult(ControllerContext context)
        {

            string filePath = "~/Uploads/Videos/" + filename;
            //var strVideoFilePath = HostingEnvironment.MapPath("~/Uploads/Videos/Test2.mp4");
            var strVideoFilePath = HostingEnvironment.MapPath(filePath);

            context.HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Test2.mp4");

            var objFile = new FileInfo(strVideoFilePath);

            var stream = objFile.OpenRead();
            var objBytes = new byte[stream.Length];
            stream.Read(objBytes, 0, (int)objFile.Length);
            context.HttpContext.Response.BinaryWrite(objBytes);

        }
    }
}