﻿using OnlineTraining.Common;
using OnlineTraining.DataModel;
using OnlineTraining.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace OnlineTraining.Controllers
{
    //[Authorize(Roles ="Admin")]
    public class AdminController : Controller
    {
        // GET: Admin
        //[Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var AdminCoursesList = new AdminCourseViewModel();
            var CoursesList = new List<CourseListModel>();
            try
            {
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                     CoursesList = dataContext.Courses.OrderBy(x => x.UpdatedDate)
                         .Select(c => new CourseListModel
                         {
                             CourseId = c.CourseId,
                             CourseName = c.CourseName,
                             Description = c.Description,
                             IsActive = c.IsActive,
                             UpdatedDate=c.UpdatedDate
                         }).Take(8).ToList();
                    AdminCoursesList.CourseList = CoursesList;
                    AdminCoursesList.TotalCourses = dataContext.Courses.Count();
                    AdminCoursesList.TotalInactiveCourses = dataContext.Courses.Where(x => x.IsActive == false).Count();
                    AdminCoursesList.TotalSections = dataContext.Sections.Count();
                    AdminCoursesList.TotalTopics = dataContext.Topics.Count();

                }
            }
            catch (Exception e)
            {
                throw e;
            }

           
            return View(AdminCoursesList);
        }
        private List<CourseListModel> GetAllCourses()
        {
            var CoursesList = new List<CourseListModel>();
            try
            {
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    CoursesList = dataContext.Courses.Where(x => x.IsActive == true)
                        .Select(c => new CourseListModel
                        {
                            CourseId = c.CourseId,
                            CourseName = c.CourseName,
                            Description = c.Description,
                            IsActive = c.IsActive,
                            UpdatedBy = c.UpdatedBy,
                            UpdatedDate = c.UpdatedDate


                        }).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return CoursesList;
        }

        public ActionResult Course()
        {
            var courses = new CourseViewModel();
            try
            {

                courses.CoursesList = GetAllCourses();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(courses);
        }
        //[HttpPost]
        //  public ActionResult Course(CourseViewModel model)
        public ActionResult SaveCourse(CourseViewModel model)
        {
            if (ModelState.IsValid)
            {
                Cours course = new Cours();
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    if (model.CourseId == 0)
                    {
                        course.CourseName = model.CourseName;
                        course.UpdatedDate = System.DateTime.Now;
                        course.UpdatedBy = "Admin";
                        course.IsActive = model.IsActive;
                        course.Description = model.Description;
                        dataContext.Courses.Add(course);
                        dataContext.SaveChanges();
                    }
                }

            }
            model.CoursesList = GetAllCourses();
            return PartialView("_CourseList", model);
            //return View(model);
        }

        #region "Sections"


        public ActionResult Sections(int CourseId, string CourseName)
        {
            SectionViewModel sectionView = new SectionViewModel();
            sectionView.CourseId = CourseId;
            sectionView.CourseName = CourseName;

            try
            {
                if (CourseId > 0)
                {
                    using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                    {
                        sectionView.SectionList = dataContext.Sections.Where(x => x.CourseId == CourseId)
                            .Select(s => new SectionViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.SectionName,
                                CourseId = s.CourseId,
                                CourseName = s.Cours.CourseName,
                                UpdatedDate = s.UpdatedDate,
                                UpdatedBy = s.UpdatedBy,
                                IsActive = s.IsActive

                            }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(sectionView);
        }

        public ActionResult SaveSection(SectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                Section section = new Section();
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    if (model.SectionId == 0)
                    {
                        section.CourseId = model.CourseId;
                        section.SectionName = model.SectionName;
                        section.UpdatedDate = System.DateTime.Now;
                        section.UpdatedBy = "Admin";
                        section.IsActive = model.IsActive;
                        dataContext.Sections.Add(section);
                        dataContext.SaveChanges();
                    }
                }

            }
            model.SectionList = GetSectionsByCourseId(model.CourseId);
            return PartialView("_SectionList", model);
            // return View(model);
        }

        private List<SectionViewModel> GetSectionsByCourseId(int CourseId)
        {
            var sectionList = new List<SectionViewModel>();
            try
            {
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    sectionList = dataContext.Sections.Where(x => x.CourseId == CourseId)
                            .Select(s => new SectionViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.SectionName,
                                CourseId = s.CourseId,
                                CourseName = s.Cours.CourseName,
                                UpdatedDate = s.UpdatedDate,
                                UpdatedBy = s.UpdatedBy,
                                IsActive = s.IsActive

                            }).ToList(); ;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return sectionList;
        }

        private List<Cours> GetAllSections()
        {
            var CoursesList = new List<Cours>();
            try
            {
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    CoursesList = dataContext.Courses.ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return CoursesList;
        }

        public ActionResult AllSections()
        {
            SectionViewModel sectionView = new SectionViewModel();

            try
            {

                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    sectionView.SectionList = dataContext.Sections
                        .Select(s => new SectionViewModel
                        {
                            SectionId = s.SectionId,
                            SectionName = s.SectionName,
                            CourseId = s.CourseId,
                            CourseName = s.Cours.CourseName,
                            UpdatedDate = s.UpdatedDate,
                            UpdatedBy = s.UpdatedBy,
                            IsActive = s.IsActive

                        }).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(sectionView);
        }

        #endregion

        #region "Topics"
        [HttpGet]
        public ActionResult AllTopics()
        {
            TopicViewModel topicView = new TopicViewModel();


            try
            {

                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    topicView.TopicList = dataContext.Topics
                        .Select(s => new TopicViewModel
                        {
                            SectionId = s.SectionId,
                            SectionName = s.Section.SectionName,
                            TopicId = s.TopicId,
                            TopicName = s.TopicName,
                            UpdatedDate = s.UpdatedDate,
                            UpdatedBy = s.UpdatedBy,
                            IsActive = s.IsActive,
                            Description = s.Description,
                            PdfLink = s.PdfLink,
                            VideoLink = s.VideoLink

                        }).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(topicView);
        }

        public ActionResult Topics(int SectionId, string SectionName)
        {
            TopicViewModel topicView = new TopicViewModel();
            topicView.SectionId = SectionId;
            topicView.SectionName = SectionName;

            try
            {
                if (SectionId > 0)
                {
                    using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                    {
                        topicView.TopicList = dataContext.Topics.Where(x => x.SectionId == SectionId)
                            .Select(s => new TopicViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.Section.SectionName,
                                TopicId = s.TopicId,
                                TopicName = s.TopicName,
                                UpdatedDate = s.UpdatedDate,
                                UpdatedBy = s.UpdatedBy,
                                IsActive = s.IsActive,
                                Description = s.Description,
                                PdfLink = s.PdfLink,
                                VideoLink = s.VideoLink

                            }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(topicView);
        }
        [HttpPost]
        public ActionResult Topics(TopicViewModel model, HttpPostedFileBase PdfLink, HttpPostedFileBase VideoLink)
        {

            string pdfFileName = string.Empty;
            string videoFileName = string.Empty;
            int MaxContentLength = 1073741824; //3 MB
            string VideoLinkpath = string.Empty;
            string PdfLinkpath = string.Empty;

            if (PdfLink == null || VideoLink == null)
            {
                ModelState.AddModelError("File", "Please Upload Your file");
                return View(model);
            }

            if (VideoLink.ContentLength > 0)
            {
                string[] AllowedFileExtensions = new string[] { ".mp4", ".avi" };

                if (!AllowedFileExtensions.Contains(VideoLink.FileName.Substring(VideoLink.FileName.LastIndexOf('.'))))
                {
                    ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                }

                else if (VideoLink.ContentLength > MaxContentLength)
                {
                    ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                }
                else
                {
                    videoFileName = Path.GetFileName(VideoLink.FileName);
                    VideoLinkpath = Path.Combine(Server.MapPath("~/Uploads/Videos"), videoFileName);

                }


                // //else
                // //{
                // //    //TO:DO
                // videoFileName = Path.GetFileName(VideoLink.FileName);
                // var path = Path.Combine(Server.MapPath("~/Uploads/Videos"), videoFileName);
                // // var path = Path.Combine(Server.MapPath("~/Uploads/Videos"), videoFileName);
                //// string path = AppDomain.CurrentDomain.BaseDirectory + "/Uploads/Videos/";
                // //if (System.IO.File.Exists(path))
                // //{
                // //    ModelState.AddModelError("File", "Filename already exists");
                // //    return View(model);
                // //}
                // PdfLink.SaveAs(path);

                // //string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                // //string filename = Path.GetFileName(Request.Files[upload].FileName);
                // //Request.Files[upload].SaveAs(Path.Combine(path, filename));

                // //}
            }
            if (PdfLink.ContentLength > 0)
            {

                string[] AllowedFileExtensions = new string[] { ".pdf" };

                if (!AllowedFileExtensions.Contains(PdfLink.FileName.Substring(PdfLink.FileName.LastIndexOf('.'))))
                {
                    ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                }

                else if (PdfLink.ContentLength > MaxContentLength)
                {
                    ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                }
                else
                {
                    //TO:DO
                    pdfFileName = Path.GetFileName(PdfLink.FileName);
                    PdfLinkpath = Path.Combine(Server.MapPath("~/Uploads/TextFiles"), pdfFileName);
                    //if (System.IO.File.Exists(PdfLinkpath))
                    //{
                    //    ModelState.AddModelError("File", "Filename already exists");
                    //    //return View(model);
                    //}

                    //ModelState.Clear();
                    //ViewBag.Message = "File uploaded successfully";
                }
            }




            if (ModelState.IsValid)
            {
                VideoLink.SaveAs(VideoLinkpath);
                PdfLink.SaveAs(PdfLinkpath);


                Topic topic = new Topic();
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    if (model.TopicId == 0)
                    {
                        topic.TopicId = model.TopicId;
                        topic.TopicName = model.TopicName;
                        topic.SectionId = model.SectionId;
                        topic.Description = "";
                        topic.PdfLink = pdfFileName;
                        topic.VideoLink = videoFileName;
                        topic.UpdatedDate = System.DateTime.Now;
                        topic.UpdatedBy = "Admin";
                        topic.IsActive = model.IsActive;
                        dataContext.Topics.Add(topic);
                        dataContext.SaveChanges();
                    }
                }

            }
            model.TopicList = GetTopicsBySectionId(model.SectionId);
            // return PartialView("_SectionList", model);
            return View(model);
        }

        public List<TopicViewModel> GetTopicsBySectionId(int SectionId)
        {
            //int SectionId = 1;
            var topicList = new TopicViewModel();
            try
            {
                using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                {
                    topicList.TopicList = dataContext.Topics.Where(x => x.SectionId == SectionId)
                            .Select(s => new TopicViewModel
                            {
                                SectionId = s.SectionId,
                                SectionName = s.Section.SectionName,
                                TopicId = s.TopicId,
                                TopicName = s.TopicName,
                                UpdatedDate = s.UpdatedDate,
                                UpdatedBy = s.UpdatedBy,
                                IsActive = s.IsActive,
                                PdfLink = s.PdfLink,
                                VideoLink = s.VideoLink,
                                Description = s.Description

                            }).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            //return PartialView("_TopicList", topicList);
            return topicList.TopicList;
        }



        public ActionResult PlayVideo(string FileName)
        {
            ViewBag.FileName = FileName;
            //ViewBag.source = "~/Uploads/Videos/Drones.mp4";
            return View();
        }

        public ActionResult PlayVideo1(string FileName)
        {

            ////The File Path
            //var videoFilePath = HostingEnvironment.MapPath("~/Uploads/Videos/Test2.mp4");
            //ControllerContext context = new ControllerContext();
            ////The header information
            ////context.HttpContext.Response.AddHeader("Content-Disposition", "attachment; filename=Test2.mp4");
            //var file = new FileInfo(videoFilePath);
            ////Check the file exist,  it will be written into the response
            //if (file.Exists)
            //{
            //    var stream = file.OpenRead();
            //    var bytesinfile = new byte[stream.Length];
            //    stream.Read(bytesinfile, 0, (int)file.Length);
            //    context.HttpContext.Response.BinaryWrite(bytesinfile);
            //}
            //return View();

            return new VideoDataResult(FileName);
        }

        public ActionResult DisplayPdf(string FileName)
        {
            ViewBag.PdfFileName = FileName;
            return View();
        }

        public FileStreamResult GetPDF(string PdfFileName)
        {
            string filePath = "~/Uploads/TextFiles/" + PdfFileName;
            // var videoFilePath = HostingEnvironment.MapPath("~/Uploads/TextFiles/ClientRequirement.pdf");
            var videoFilePath = HostingEnvironment.MapPath(filePath);
            //Server.MapPath(@"~\HTP.pdf")
            FileStream fs = new FileStream(videoFilePath, FileMode.Open, FileAccess.Read);
            return File(fs, "application/pdf");
        }
        #endregion
        public SectionsTopics gettopicssectionsbycourseId(int CourseId)
        {
            var sectionsTopics = new SectionsTopics();
            using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
            {
                sectionsTopics = dataContext.Sections.Where(x => x.CourseId == CourseId)
                    .Select(c => new SectionsTopics
                    {
                        SectionId = c.SectionId,
                        SectionName = c.SectionName,
                        //topisBySections=c.Topics.ToList()


                    }).SingleOrDefault();
            }
            return sectionsTopics;
        }
        public ActionResult GetCourse(int CourseId)
        {
            CourseSectionsTopics courseSectionsTopics = new CourseSectionsTopics();

            try
            {
                if (CourseId > 0)
                {
                    using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                    {
                        courseSectionsTopics = dataContext.Courses.Where(x => x.CourseId == CourseId)
                            .Select(c => new CourseSectionsTopics
                            {
                                CourseId = c.CourseId,
                                CourseName = c.CourseName,
                                Description = c.Description,
                                Sections=c.Sections.ToList(),
                                //SectionsTopics=gettopicssectionsbycourseId(CourseId)
                                //SectionsTopics = dataContext.Sections.Where(x => x.CourseId == CourseId)
                                //.Select(s => new SectionsTopics
                                //{
                                //    SectionId = s.SectionId,
                                //    SectionName = s.SectionName,
                                //    //topisBySections=c.Topics.ToList()
                                //}).FirstOrDefault()

                    }).SingleOrDefault();
                    }

                    var SectionsTopics = new SectionsTopics();
                    var LstSectionsTopics = new List<SectionsTopics>();
                    if (courseSectionsTopics.Sections.Count > 0)
                    {
                        foreach (var sec in courseSectionsTopics.Sections)
                        {
                            using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
                            {
                                SectionsTopics = dataContext.Sections.Where(x => x.SectionId == sec.SectionId)
                                    .Select(c => new SectionsTopics
                                    {
                                        SectionId = c.SectionId,
                                        SectionName = c.SectionName,
                                        topisBySections = c.Topics.ToList()

                                    }).SingleOrDefault();
                                LstSectionsTopics.Add(SectionsTopics);
                                courseSectionsTopics.LstSectionsTopics = LstSectionsTopics;

                                //courseSectionsTopics.SectionsTopics = SectionsTopics;

                            }
                        }
                    }
                    else
                    {
                        courseSectionsTopics.LstSectionsTopics = new List<SectionsTopics>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(courseSectionsTopics);
        }


        public ActionResult Authors()
        {
            //var AdminCoursesList = new AdminCourseViewModel();
            //var CoursesList = new List<CourseListModel>();
            //try
            //{
            //    using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
            //    {
            //        CoursesList = dataContext.Courses.OrderBy(x => x.UpdatedDate)
            //            .Select(c => new CourseListModel
            //            {
            //                CourseId = c.CourseId,
            //                CourseName = c.CourseName,
            //                Description = c.Description,
            //                IsActive = c.IsActive,
            //                UpdatedDate = c.UpdatedDate
            //            }).Take(8).ToList();
            //        AdminCoursesList.CourseList = CoursesList;
            //        AdminCoursesList.TotalCourses = dataContext.Courses.Count();
            //        AdminCoursesList.TotalInactiveCourses = dataContext.Courses.Where(x => x.IsActive == false).Count();
            //        AdminCoursesList.TotalSections = dataContext.Sections.Count();
            //        AdminCoursesList.TotalTopics = dataContext.Topics.Count();

            //    }
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}


            // return View(AdminCoursesList);
            return View();
        }

        public ActionResult Users()
        {
            using (OnlineTrainingEntities dataContext = new OnlineTrainingEntities())
            {




                //var customUser = (from u in dataContext.AspNetUsers
                //                  join r in dataContext.AspNetUserRole on u.Id equals pr.userId
                //                  where pr.url == token && pr.requestDateTime < dt
                //                  select u).First()

                //var users = dataContext.AspNetUsers.Join as JavaScript 
                //        .Select(u => new UsersViewModel
                //        {
                //            UserName = u.UserName,
                //            FirstName = u.FirstName,
                //            LastName = u.Description,
                //            IsActive = u.IsActive,
                //            Email = u.Email,
                //        }).ToList();
            }
            return View();
        }
    }



}
