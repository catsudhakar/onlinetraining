﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OnlineTraining.Models
{
    public class CustomAuthorize : ActionFilterAttribute
    {
        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
        //    {
        //        filterContext.Result = new HttpUnauthorizedResult();
        //    }
        //    else
        //    {
        //        filterContext.Result = new RedirectToRouteResult(new
        //            RouteValueDictionary(new { controller = "LogIn" }));
        //    }
        //}

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (!IsAuthorized(filterContext))
            {
                filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccessDenied" }));
            }
        }

        private bool IsAuthorized(ActionExecutingContext filterContext)
        {
            var descriptor = filterContext.ActionDescriptor;
            var authorizeAttr = descriptor.GetCustomAttributes(typeof(AuthorizeAttribute), false).FirstOrDefault() as AuthorizeAttribute;

            if (authorizeAttr != null)
            {
                if (!authorizeAttr.Users.Contains(filterContext.HttpContext.User.ToString()))
                    return false;
            }
            return true;

        }
    }
}

